<?php

namespace Drupal\mouseflow\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;

/**
 * Configure Mouseflow admin settings for this site.
 */
class MouseflowAdminSettingsForm extends ConfigFormBase {
    /**
     * {inheritdoc}
     */
    public function getFormID() {
        return 'mouseflow_admin_settings';
    }

    /**
     * {inheritdoc}
     */
    protected function getEditableConfigNames() {
        return [
            'mouseflow.settings'
        ];
    }

    /**
     * {inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $config = $this->config('mouseflow.settings');

        $form['mouseflow_code_form'] = array(
            '#type' => 'textarea',
            '#title' => t('Please add your Mouseflow JS code (without &lt;script&gt; tags)'),
            '#default_value' => $config->get('mouseflow_code'),
        );

        return parent::buildForm($form, $form_state);
    }

    /**
     * {inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        $this
            ->config('mouseflow.settings')
            ->set('mouseflow_code', $form_state->getValue('mouseflow_code_form'))
            ->save();

        parent::submitForm($form, $form_state);
    }
}
